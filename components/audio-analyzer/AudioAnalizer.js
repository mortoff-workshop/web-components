export default class AudioAnalyzer extends HTMLElement {

    constructor() {
        super();

        this.src = this.getAttribute('src');
        this.resolution = Number(this.getAttribute('resolution')) || 64;
        this.updateRate = Number(this.getAttribute('updateRate')) || 20;

        const shadow = this.attachShadow({ mode: 'open' });

        const audio = new Audio();
        audio.src = this.src;
        this.audio = audio;

        document.addEventListener('click', event => {
            if (!this.started) {
                const audioContext = new AudioContext();
                const audioSource = audioContext.createMediaElementSource(audio);
                
                const analyzer = audioContext.createAnalyser();
                audioSource.connect(analyzer);
                analyzer.connect(audioContext.destination);
                analyzer.fftSize = this.resolution * 2;
                this.analyzer = analyzer;
                
                this.dataArray = new Uint8Array(analyzer.frequencyBinCount);
                
                this.started = true;
                this.play();
            } else if (!this.playing) {
                this.play();
            } else {
                this.pause();
            }
            
        });
    }

    play() {
        this.audio.play();
        this.playing = true;
        const streamData = () => {
            setTimeout(() => {
                this.analyzer.getByteFrequencyData(this.dataArray);
                this.dispatchEvent(new CustomEvent('audio-analyzer-data', { detail: this.dataArray }));
                if (this.playing) {
                    streamData();
                }
            }, this.updateRate);
        };
        streamData();
    }

    pause() {
        this.audio.pause();
        this.playing = false;
    }
}

customElements.define('audio-analyzer', AudioAnalyzer);
