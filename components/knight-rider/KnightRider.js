import AudioAnalyzer from "/components/audio-analyzer/AudioAnalizer.js";
import LedBar from "/components/led-bar/LedBar.js";

export default class KnightRider extends HTMLElement {

    async connectedCallback() {
        const shadow = this.attachShadow({mode: 'open'});
        shadow.innerHTML = await fetch('components/knight-rider/KnightRider.html').then(response => response.text());

        shadow.appendChild(shadow.querySelector('template').content.cloneNode(true));
        
        const spectroVis = shadow.querySelector('#spectro');
        const blinker = shadow.querySelector('#blinker');
        const analyzer = shadow.querySelector('#analyzer');
        
        analyzer.addEventListener('audio-analyzer-data', event => {
            const normal = Math.min(Math.max(event.detail[0] - 215, 0) / 40, 1);
            const leds = Math.floor(spectroVis.numTilesInRow * normal);
            spectroVis.setAttribute('current', leds);
            blinker.setAttribute('current', normal > 0.9 ? 1 : 0);
        });

        const leftRight = shadow.querySelector('#left-right');
        let direction = 1;
        let position = 0;
        const moveLeftRight = () => {
            setTimeout(() => {
                leftRight.setAttribute('current', position);
                if (Math.abs(position) >= leftRight.numTilesInRow) {
                    position *= -1;
                }
                position += direction;
                if (position == 0) {
                    direction *= -1;
                }
                moveLeftRight();
            }, 120);
        };
        moveLeftRight();
    }

}

customElements.define('knight-rider', KnightRider);
