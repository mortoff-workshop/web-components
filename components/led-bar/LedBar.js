export default class LedBar extends HTMLElement {

    static get observedAttributes() { return ['current']; }

    constructor() {
        super();

        this.width = Number(this.getAttribute("width")) || 800;
        this.height = Number(this.getAttribute("height")) || 100;
        
        this.orientation = this.getAttribute("orientation") || 'horizontal';
        this.numTilesInRow = Number(this.getAttribute("numTilesInRow")) || 10;
        this.numRows = Number(this.getAttribute("numRows")) || 2;
        this.gap = Number(this.getAttribute("gap")) || 2;
        this.background = this.getAttribute("background") || 'black';
        this.color = this.getAttribute("color") || 'red';

        const shadow = this.attachShadow({ mode: 'open' });

        const style = document.createElement('style');
        shadow.appendChild(style);
        
        style.textContent = `
            div, canvas {
                width: ${this.width}px;
                height: ${this.height}px;
            }
        
            div#wrapper {
                position: relative;
                top: 0;
                left: 0;
                margin: 20px auto;
            }
        
            canvas {
                position: absolute;
                top: 0;
                left: 0;
            }
        `;

        const wrapper = document.createElement('div');
        wrapper.id = 'wrapper';
        shadow.appendChild(wrapper);

        this.canvas = document.createElement('canvas');
        wrapper.appendChild(this.canvas);
        this.initCanvas(this.canvas);

        this.mask = document.createElement('canvas');
        wrapper.appendChild(this.mask);
        this.mask.width = this.width;
        this.mask.height = this.height;

        this.goDark();
    }

    initCanvas() {
        const canvas = this.canvas;
        canvas.width = this.width;
        canvas.height = this.height;

        const ctx = canvas.getContext('2d');
        ctx.fillStyle = this.color;
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        if (this.orientation !== 'horizontal') {
            ctx.transform(0, this.height / this.width, this.width / this.height, 0, 0, 0);
        }

        if (this.numRows > 1) {
            for (const row of Array(this.numRows + 1).keys()) {
                const y = this.height / this.numRows * (row);
                ctx.strokeStyle = this.background;
                ctx.lineWidth = this.gap;
                ctx.beginPath();
                ctx.moveTo(0, y);
                ctx.lineTo(this.width, y);
                ctx.stroke();
            }
        }

        if (this.numTilesInRow > 1) {
            for (const tile of Array(this.numTilesInRow + 1).keys()) {
                const x = this.width / this.numTilesInRow * (tile);
                ctx.strokeStyle = this.background;
                ctx.lineWidth = this.gap;
                ctx.beginPath();
                ctx.moveTo(x, 0);
                ctx.lineTo(x, this.height);
                ctx.stroke();
            }
        }
    }

    goDark() {
        const ctx = this.mask.getContext('2d');
        ctx.fillStyle = 'rgba(0, 0, 0, 0.7)';
        ctx.fillRect(0, 0, this.width, this.height);
    }

    show(n) {
        const ctx = this.mask.getContext('2d');
        ctx.save();
        ctx.clearRect(0, 0, this.width, this.height);
        ctx.translate(this.width / this.numTilesInRow * n, 0);
        this.goDark();
        ctx.restore();
    }

    attributeChangedCallback(name, oldValue, newValue) {
        this.show(Number(newValue));
    }

}

customElements.define('led-bar', LedBar);
