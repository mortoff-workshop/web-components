### Knight Rider web komponens fejlesztése

Megjelenítjük KITT "pajti" piros fénycsíkjait, vagy annál kicsit többet.

Hasselhoff (Májköl) most nem lesz velünk, csak ha lezáratlanul hagyjuk a laptopjainkat a szünetben.

### Előkészületek

- IDE: [Visual Studio Code](https://code.visualstudio.com/)
- Git: [for Windows](https://git-scm.com/download/win)
- HTTP szerver: [Node.js](https://nodejs.org/en/download/) + [http-server](https://www.npmjs.com/package/http-server)

A fentiek helyett bármilyen kényelmes alternatíva használható.

Pl. Live Server Extension a http-server helyett, WebStorm, stb.

http-server telepítés és indítás:
``` bash
npm i -g http-server
http-server
```

### Azure-ban futó példány

https://mortoffworkshop.z20.web.core.windows.net/

### Felhasznált irodalom

- https://developer.mozilla.org/en-US/docs/Web/Web_Components
- https://blog.logrocket.com/audio-visualizer-from-scratch-javascript/
